PKG=progimage.com
PKGLIST=$(shell go list ${PKG}/... | grep -v /vendor/vendor)
ROOT=$(shell git rev-parse --show-toplevel)
TMP_UPLOAD_DIR=${ROOT}/tmp-files

localapp: clean
	# @mkdir -p ${TMP_UPLOAD_DIR}
	go run app/main.go -td=${TMP_UPLOAD_DIR}


resource-down:
	docker-compose -f docker-resource.yml down
resource: resource-down
	docker-compose -f docker-resource.yml up -d


# for local development :)
dockerup: dockerdown
	docker-compose up --build -d
dockerdown:
	docker-compose rm -f
	docker-compose down

# containerized everything -> WIP
containerup: containerdown
	docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
containerdown:
	docker-compose -f docker-compose.yml -f docker-compose.prod.yml down

test: clean
	@go test -short -cover ${PKGLIST}

cover: clean
	@go test -cover ${PKGLIST}

clean: 
	@go clean -testcache
	@rm -rf ${TMP_UPLOAD_DIR}
dep: 
	go mod download
tidy:
	go mod tidy 
	go get -t .

security: 
	gosec -quiet -exclude=G304,G307 -severity=medium ./...