module progimage.com

go 1.16

require (
	github.com/aws/aws-sdk-go v1.38.60
	github.com/disintegration/imaging v1.6.2
	github.com/go-kit/kit v0.10.0
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/go-xorm/xorm v0.7.9
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/gorilla/mux v1.8.0
	github.com/jasonlvhit/gocron v0.0.1
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.2
	github.com/newrelic/go-agent/v3 v3.13.0
	github.com/newrelic/go-agent/v3/integrations/nrpq v1.1.1
	github.com/onsi/ginkgo v1.16.4
	github.com/onsi/gomega v1.13.0
	github.com/stretchr/testify v1.7.0
	xorm.io/builder v0.3.6
)
