package main

import (
	"fmt"
	"log"
	"os"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"progimage.com/utils/db"

	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
)

const (
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbName   = "progimage"
)

var (
	host = "localhost"
)

func main() {
	fmt.Println("start migration")
	if h := os.Getenv("PGHOST"); h != "" {
		host = h
	}
	engine, err := db.New(host, user, password, dbName, port)
	if err != nil {
		log.Fatal(err)
	}
	defer engine.Close()
	driver, err := postgres.WithInstance(engine.DB().DB, &postgres.Config{})
	if err != nil {
		panic(err)
	}
	m, err := migrate.NewWithDatabaseInstance(
		"file://db",
		"postgres", driver)
	if err != nil {
		fmt.Println("error isntance", err.Error())
	}
	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		panic(err)
	}
	fmt.Println("end migration")
}
