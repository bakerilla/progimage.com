CREATE TABLE file_info(
   id uuid,
   date_created timestamptz,
   uploaded boolean,
   source text,
   PRIMARY KEY( id)
);

