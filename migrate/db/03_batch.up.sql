
CREATE TYPE batch_status as ENUM('pending','started', 'error', 'completed', 'ready');
CREATE TABLE batch_transformation(
   id uuid,
   extension text,
   parameters json,
   file_ids json, 
   date_created timestamptz,
   bstatus batch_status,
   PRIMARY KEY(id)
);

