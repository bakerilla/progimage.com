CREATE TABLE file_transformation(
   id uuid,
   transformation_type text,
   parameters text, 
   date_created timestamptz,
   PRIMARY KEY(id, transformation_type, parameters)
);

