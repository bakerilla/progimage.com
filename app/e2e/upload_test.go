package e2e_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"image"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"

	_ "image/gif"
	"image/jpeg"
	_ "image/jpeg"
	_ "image/png"

	"github.com/disintegration/imaging"
	. "github.com/onsi/ginkgo"
	"github.com/stretchr/testify/assert"
	"progimage.com/app/models"
	"progimage.com/app/transform"
)

var _ = Describe("Upload and Download", func() {
	var a *assert.Assertions
	BeforeEach(func() {
		fmt.Println("serve rurl", serverURL)
		a = assert.New(GinkgoT())
	})

	Context("test upload and download jpg", func() {
		var fileInfo *models.FileInfo

		AfterEach(func() {
			var validator models.FileInfo
			ok, err := engine.Table(models.FileInfoTableName).Where("id=? and uploaded=?", fileInfo.ID, true).Get(&validator)
			a.NoError(err)
			a.True(ok)

			//download
			f, err := os.Open("pika.jpg")
			a.NoError(err)
			img, _, err := image.Decode(f)
			a.NoError(err)
			buf := bytes.Buffer{}
			err = jpeg.Encode(&buf, img, nil)
			a.NoError(err)
			b := downloadFile(a, validator.ID, "jpg")
			a.True(bytes.Equal(buf.Bytes(), b))
		})
		It("test upload from file", func() {
			fileInfo = uploadFile(a, "file", "pika.jpg")
		})

		It("test upload from url", func() {
			fileInfo = uploadFile(a, "url", cdnURL)
		})
	})

	Context("test upload jpg and download png", func() {
		var fileInfo *models.FileInfo
		AfterEach(func() {
			var validator models.FileInfo
			ok, err := engine.Table(models.FileInfoTableName).Where("id=? and uploaded=?", fileInfo.ID, true).Get(&validator)
			a.NoError(err)
			a.True(ok)

			//download
			sourcefile, err := ioutil.ReadFile("pika.jpg")
			a.NoError(err)
			img, err := transform.New(sourcefile)
			a.NoError(err)
			sourcefile, err = img.Encode(imaging.PNG)
			a.NoError(err)
			b := downloadFile(a, validator.ID, "png")
			a.True(bytes.Equal(sourcefile, b))

		})
		It("test upload from file", func() {
			fileInfo = uploadFile(a, "file", "pika.jpg")
		})

		It("test upload from url", func() {
			fileInfo = uploadFile(a, "url", cdnURL)
		})
	})

	Context("test upload jpg and download resized png", func() {
		var fileInfo *models.FileInfo
		AfterEach(func() {
			var validator models.FileInfo
			ok, err := engine.Table(models.FileInfoTableName).Where("id=? and uploaded=?", fileInfo.ID, true).Get(&validator)
			a.NoError(err)
			a.True(ok)
			resizeParam := `h_100,blur_0.5,w_100`
			extendedParams, err := transform.NewExtendedParameters(resizeParam)
			a.NoError(err)

			//download
			sourcefile, err := ioutil.ReadFile("pika.jpg")
			a.NoError(err)
			img, err := transform.New(sourcefile)
			a.NoError(err)
			img.Transform(extendedParams)

			sourcefile, err = img.Encode(imaging.PNG)
			a.NoError(err)
			b := downloadFile(a, validator.ID, "png", resizeParam)
			a.True(bytes.Equal(sourcefile, b))

		})
		It("test upload from file", func() {
			fileInfo = uploadFile(a, "file", "pika.jpg")
		})

		It("test upload from url", func() {
			fileInfo = uploadFile(a, "url", cdnURL)
		})
	})
})

func downloadFile(a *assert.Assertions, filename, extension string, transform ...string) []byte {
	url := serverURL + "/download/" + filename + "." + extension
	if len(transform) != 0 {
		stringer := transform[0]
		url = serverURL + "/download/" + stringer + "/" + filename + "." + extension
	}
	uploadUrlFunc := func(url string) *http.Response {
		request, err := http.NewRequest("GET", url, nil)
		a.NoError(err)
		response, err := (&http.Client{}).Do(request)
		a.NoError(err)
		return response
	}

	read := func(rc io.ReadCloser) []byte {
		defer rc.Close()
		b, err := ioutil.ReadAll(rc)
		a.NoError(err)
		return b
	}
	response := uploadUrlFunc(url)

	return read(response.Body)
}

func uploadFile(a *assert.Assertions, uploadType, filePath string) *models.FileInfo {
	var fi models.FileInfo
	uploadFileFunc := func(input string) *http.Response {
		url := serverURL + "/upload"
		file, err := os.Open(input)
		a.NoError(err)

		body := &bytes.Buffer{}
		writer := multipart.NewWriter(body)
		part, err := writer.CreateFormFile("file", filepath.Base(input))
		_, err = io.Copy(part, file)
		a.NoError(err)

		err = writer.Close() //<-- important
		a.NoError(err)

		request, err := http.NewRequest("POST", url, body)

		a.NoError(err)
		request.Header.Add("Content-Type", writer.FormDataContentType())
		fmt.Println(request.Header)
		response, err := (&http.Client{}).Do(request)
		a.NoError(err)
		return response
	}
	uploadUrlFunc := func(url string) *http.Response {
		u := serverURL + "/fetch?url=" + url
		request, err := http.NewRequest("GET", u, nil)
		a.NoError(err)
		response, err := (&http.Client{}).Do(request)
		a.NoError(err)
		return response
	}

	read := func(rc io.ReadCloser, i interface{}) {
		defer rc.Close()
		b, err := ioutil.ReadAll(rc)
		a.NoError(err)
		a.NoError(json.Unmarshal(b, i))
	}
	var resp *http.Response
	switch uploadType {
	case "file":
		resp = uploadFileFunc(filePath)

	case "url":
		resp = uploadUrlFunc(filePath)
	}

	defer resp.Body.Close()
	read(resp.Body, &fi)

	return &fi
}
