package e2e_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/go-kit/kit/log"
	"github.com/go-xorm/xorm"
	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"progimage.com/app/service"
	"progimage.com/app/service/batch"
	"progimage.com/app/service/download"
	"progimage.com/app/service/upload"
	"progimage.com/utils/db"
)

func TestE2e(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "E2e Suite")
}

var _ = BeforeSuite(func() {
	var err error
	if engine, err = db.New(host, user, password, dbName, port); err != nil {
		panic(err)
	}
	creds := credentials.NewStaticCredentials(AccessKeyId, SecretAccessKey, "")

	if s3Sesh, err = session.NewSession(&aws.Config{
		Endpoint: aws.String(
			fmt.Sprintf(
				"http://%s:%v",
				minioHost,
				minioPort,
			),
		),
		Region:           aws.String("us-east-1"),
		DisableSSL:       aws.Bool(true),
		S3ForcePathStyle: aws.Bool(true),
		Credentials:      creds,
	}); err != nil {
		panic(err)
	}

	tmpDir = uuid.Must(uuid.NewV4()).String()
	err = os.MkdirAll(tmpDir, 0750)
	if err != nil {
		panic(err)
	}
	upload.TmpDir = tmpDir

	//bucket
	service.InitializeBucket(s3Sesh)

	logger := log.NewLogfmtLogger(os.Stderr)
	apiLogger := log.With(logger, "listen", "8081", "caller", log.DefaultCaller)
	r := mux.NewRouter()

	uploader := s3manager.NewUploader(s3Sesh)
	downloader := s3manager.NewDownloader(s3Sesh)
	upload.NewService(engine, uploader).Route(r, log.With(apiLogger, "service", "upload"))
	download.NewService(engine, downloader).Route(r, log.With(apiLogger, "service", "download"))
	batch.NewService(engine, downloader).Route(r, log.With(apiLogger, "service", "batch"))

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		f, err := ioutil.ReadFile("pika.jpg")
		if err != nil {
			panic(err)
		}
		w.Write([]byte(f))
	}))
	cdnURL = ts.URL

	testServer := httptest.NewServer(r)

	serverURL = testServer.URL
})

var _ = AfterSuite(func() {
	os.Remove(tmpDir)
})

const (
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbName   = "progimage"
)

var (
	host      = "localhost"
	tmpDir    string
	minioHost = "localhost"
	minioPort = "9000"
)

//AWS
var (
	Region          = "us-west-1"
	AccessKeyId     = "minio"
	SecretAccessKey = "minio123"
	s3Sesh          *session.Session
	engine          *xorm.Engine

	cdnURL    string
	serverURL string
)
