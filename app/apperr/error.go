package apperr

import (
	"net/http"
)

type ErrCode int

const (
	_ = ErrCode(iota)
	ErrCodeBadRequest
	ErrCodeNotFound
)

var httpErrors = map[ErrCode]int{
	ErrCodeBadRequest: http.StatusBadRequest,
	ErrCodeNotFound:   http.StatusNotFound,
}

type Error struct {
	code    ErrCode
	message string
}

func New(code ErrCode, message ...string) *Error {
	var msg string
	if len(message) != 0 {
		msg = message[0]
	}
	return &Error{code, msg}
}

func (e *Error) Error() string {
	return e.message
}
func (e *Error) HTTPStatusCode() int {
	if v, ok := httpErrors[e.code]; ok {
		return v
	}

	return http.StatusInternalServerError
}
