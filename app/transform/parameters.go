package transform

import (
	"fmt"
	"strings"

	"progimage.com/app/apperr"
)

type ExtendedParameters struct {
	Order      []TransformationType
	Parameters Parameters
}

func NewExtendedParameters(s string) (*ExtendedParameters, error) {
	e, err := newExtendedParameters(s)
	if err != nil {
		return nil, err
	}
	return e.format(), nil
}

func newExtendedParameters(s string) (*ExtendedParameters, error) {
	var (
		params = make(Parameters)
		order  []TransformationType
	)
	if len(s) != 0 {

		splitted := strings.Split(s, ",")
		for _, entry := range splitted {
			tmpSplit := strings.Split(entry, "_")
			if len(tmpSplit) != 2 {
				return nil, apperr.New(apperr.ErrCodeBadRequest, "malformed transformation string")
			}
			if _, ok := params[TransformationType(tmpSplit[0])]; !ok {
				params[TransformationType(tmpSplit[0])] = tmpSplit[1]
				order = append(order, TransformationType(tmpSplit[0]))
			}

		}
	}
	return &ExtendedParameters{
		Parameters: params,
		Order:      order,
	}, nil
}

func (me *ExtendedParameters) format() *ExtendedParameters {
	width, hasWidth := me.Parameters[TransformationWidth]
	height, hasHeight := me.Parameters[TransformationHeight]
	crop := false
	if cropType, ok := me.Parameters[TransformationCrop]; ok {
		switch cropType {
		case "a":
		default:

			if hasWidth && hasHeight {
				me.replaceKey(TransformationCrop, transformationCropCentre, fmt.Sprintf("%s_%s", width, height))
				me.removeKey(TransformationWidth)
				me.removeKey(TransformationHeight)
				crop = true
			} else {
				me.removeKey(TransformationCrop)
			}
		}
	}
	if hasWidth && hasHeight && !crop {
		h, w := me.find(TransformationHeight), me.find(TransformationWidth)
		minVal := min(h, w)
		if minVal == h {
			//height is first
			// replace height with resize
			me.replaceKey(TransformationHeight, transformationResize, fmt.Sprintf("%s_%s", width, height))
			me.removeKey(TransformationHeight)
			me.removeKey(TransformationWidth)

		} else {
			me.replaceKey(TransformationWidth, transformationResize, fmt.Sprintf("%s_%s", width, height))
			me.removeKey(TransformationWidth)
			me.removeKey(TransformationHeight)
		}

		// p[transformationResize] = fmt.Sprintf("%s_%s", width, height)

	}
	return me

}

func (me *ExtendedParameters) replaceKey(source, replacement TransformationType, value string) {
	me.Parameters[replacement] = value
	delete(me.Parameters, source)
	// replace in array
	for i, value := range me.Order {
		if value == source {
			me.Order[i] = replacement
		}

	}

}

func (me *ExtendedParameters) removeKey(s TransformationType) {

	delete(me.Parameters, s)

	list := me.Order
	for i, v := range list {
		if v == s {
			// fmt.Println("s", s)
			list = remove(list, i)
			break
		}

	}
	me.Order = list
}

func min(a, b int) int {
	if a > b {
		return b
	}
	return a
}
func (me *ExtendedParameters) find(s TransformationType) int {
	for i, v := range me.Order {
		if v == s {
			return i
		}
	}
	return -1
}

func remove(slice []TransformationType, s int) []TransformationType {
	if s < 0 {
		return slice
	}
	return append(slice[:s], slice[s+1:]...)
}
