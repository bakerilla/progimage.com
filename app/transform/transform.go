package transform

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"image/gif"
	"image/jpeg"
	"image/png"
	"strconv"
	"strings"

	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"

	"github.com/disintegration/imaging"
	"progimage.com/app/apperr"
)

type TransformationType string

const (
	TransformationBlur       = TransformationType("blur")       // blur:{sigma}, sigma >0
	TransformationSharpen    = TransformationType("sharpen")    //sharpen:{sigma}, sigma>0
	TransformationCrop       = TransformationType("crop")       // crop:c or crop:a
	TransformationWidth      = TransformationType("width")      //width:{w}
	TransformationHeight     = TransformationType("height")     //height:{h}
	TransformationRotate     = TransformationType("rotate")     //rotate:{h}
	TransformationGamma      = TransformationType("gamma")      //gamma:{+0}
	TransformationContrast   = TransformationType("contrast")   //contrast:{+-0}
	TransformationBrightness = TransformationType("brightness") //brightness:{+-0}
	TransformationSaturation = TransformationType("saturation") //brightness:{+-0}

	//derived transformation for resizing : requires width, height
	transformationResize = TransformationType("resize")
	//derived transformation for croping
	transformationCropCentre = TransformationType("crop_center")
	transformationCropAnchor = TransformationType("crop_anchor")
)

// exampel fo string: blur_0.5,sharpen_1.5
type Parameters map[TransformationType]string

type Transformation struct {
	originalData []byte
	image        image.Image
	filter       imaging.ResampleFilter
	format       imaging.Format
}

func (t Transformation) Image() image.Image {
	return t.image
}

func New(b []byte) (*Transformation, error) {
	return new(b)
}
func new(b []byte) (*Transformation, error) {

	img, format, err := image.Decode(bytes.NewReader(b))
	if err != nil {
		return nil, err
	}
	fm, err := imaging.FormatFromExtension(format)
	if err != nil {
		return nil, err
	}

	return &Transformation{
		image:        img,
		originalData: b,
		filter:       imaging.Lanczos,
		format:       fm,
	}, nil
}

func (me *Transformation) Transform(ep *ExtendedParameters, must ...bool) image.Image {
	params := ep.Parameters
	order := ep.Order
	for _, k := range order {
		var (
			glug error
		)

		v, ok := params[k]
		if !ok {
			continue
		}
		switch k {
		case TransformationCrop:
		case TransformationBlur, TransformationSharpen:
			img, err := sharpenOrBlur(me.image, k, v)
			if err != nil {
				fmt.Println("failed to process ", err.Error())
				glug = err
			}
			me.image = img
		case transformationCropCentre:
			values := strings.Split(v, "_")
			w, _ := strconv.Atoi(values[0])
			h, _ := strconv.Atoi(values[1])
			me.image = imaging.CropCenter(me.image, w, h)
		case transformationResize:
			values := strings.Split(v, "_")
			w, _ := strconv.Atoi(values[0])
			h, _ := strconv.Atoi(values[1])
			me.image = imaging.Resize(me.image, w, h, me.filter)
		case TransformationWidth:
			w, _ := strconv.Atoi(v)
			me.image = imaging.Resize(me.image, w, 0, me.filter)
		case TransformationHeight:
			h, _ := strconv.Atoi(v)
			me.image = imaging.Resize(me.image, 0, h, me.filter)
		case TransformationRotate:
			sigma, err := strconv.ParseFloat(v, 32)
			if err == nil {
				me.image = imaging.Rotate(me.image, sigma, color.Black)
			} else {
				glug = err
			}
		case TransformationGamma:
			sigma, err := strconv.ParseFloat(v, 32)
			if err == nil && sigma >= 0 {
				me.image = imaging.AdjustGamma(me.image, sigma)
			} else {
				glug = err
			}
		case TransformationContrast:
			sigma, err := strconv.ParseFloat(v, 32)
			if err == nil {
				me.image = imaging.AdjustContrast(me.image, sigma)
			} else {
				glug = err
			}
		case TransformationBrightness:
			sigma, err := strconv.ParseFloat(v, 32)
			if err == nil {
				me.image = imaging.AdjustBrightness(me.image, sigma)
			} else {
				glug = err
			}
		case TransformationSaturation:
			sigma, err := strconv.ParseFloat(v, 32)
			if err == nil {
				me.image = imaging.AdjustSaturation(me.image, sigma)
			} else {
				glug = err
			}

		}
		if glug != nil {

		}

	}
	return me.image
}

func sharpenOrBlur(img image.Image, key TransformationType, value string) (image.Image, error) {
	sigma, err := strconv.ParseFloat(value, 32)
	if err != nil || sigma < 0 {
		fmt.Println("failed to compute sigma for ", key)
		return img, err
	}
	switch key {
	case TransformationBlur:
		return imaging.Blur(img, sigma), nil
	case TransformationSharpen:
		return imaging.Sharpen(img, sigma), nil

	}
	return img, nil

}

func (me *Transformation) Encode(format imaging.Format) (b []byte, err error) {

	buf := bytes.Buffer{}

	switch format {
	case imaging.JPEG:
		err := jpeg.Encode(&buf, me.image, nil)
		if err != nil {
			return nil, err
		}
		return buf.Bytes(), nil
	case imaging.PNG:
		err := png.Encode(&buf, me.image)
		if err != nil {
			return nil, err
		}
	case imaging.GIF:
		err := gif.Encode(&buf, me.image, nil)
		if err != nil {
			return nil, err
		}
	default:
		return nil, apperr.New(apperr.ErrCodeBadRequest, "unsupported file format")
	}

	return buf.Bytes(), nil
}
