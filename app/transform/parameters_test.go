package transform_test

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"progimage.com/app/transform"
)

func Test_ExtendedParams(t *testing.T) {

	testExtendedParams := func(input string, expectations []transform.TransformationType) {
		x, err := transform.NewExtendedParameters(input)
		assert.NoError(t, err)
		fmt.Println(input, x)
		assert.True(t, reflect.DeepEqual(expectations, x.Order))
	}
	testExtendedParams(`blur_25,width_105,crop_c,width_100,height_100,crop_a`,
		[]transform.TransformationType{
			transform.TransformationBlur,
			transform.TransformationType("crop_center"),
		},
	)
	testExtendedParams(`blur_25,width_105,width_100,height_100,crop_c`,
		[]transform.TransformationType{
			transform.TransformationBlur,
			transform.TransformationType("crop_center"),
		},
	)
	testExtendedParams(`blur_25,width_105,sharpen_122,height_100`,
		[]transform.TransformationType{
			transform.TransformationBlur,
			transform.TransformationType("resize"),
			transform.TransformationType("sharpen"),
		},
	)
	testExtendedParams(`blur_25,width_105,sharpen_122,rotate_100`,
		[]transform.TransformationType{
			transform.TransformationBlur,
			transform.TransformationType("width"),
			transform.TransformationType("sharpen"),
			transform.TransformationType("rotate"),
		},
	)
}
