package transform_test

import (
	"bytes"
	"image"
	"image/color"
	"io/ioutil"
	"os"
	"testing"

	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"

	"github.com/disintegration/imaging"
	"github.com/stretchr/testify/assert"
	. "progimage.com/app/transform"
)

func Test_Transform_CropCentre(t *testing.T) {
	assert := assert.New(t)
	//control
	f, err := os.Open("sample.png")
	assert.NoError(err)
	controlImage, _, err := image.Decode(f)
	assert.NoError(err)
	controlImage = imaging.CropCenter(controlImage, 100, 100)
	assert.NoError(err)
	buf := bytes.Buffer{}
	assert.NoError(imaging.Encode(&buf, controlImage, imaging.PNG))

	controlData := buf.Bytes()

	// function
	paramString := `crop_c,width_100,height_100`
	params, err := NewExtendedParameters(paramString)
	assert.NoError(err)
	data, err := ioutil.ReadFile("sample.png")
	assert.NoError(err)
	transform, err := New(data)
	assert.NoError(err)
	transform.Transform(params)
	transformeData, err := transform.Encode(imaging.PNG)

	assert.True(bytes.Equal(controlData, transformeData))
}
func Test_Transform_Sharpen(t *testing.T) {
	assert := assert.New(t)
	//control
	f, err := os.Open("sample.png")
	assert.NoError(err)
	controlImage, err := imaging.Decode(f)
	assert.NoError(err)
	controlImage = imaging.Sharpen(controlImage, 100)
	assert.NoError(err)
	buf := bytes.Buffer{}
	assert.NoError(imaging.Encode(&buf, controlImage, imaging.PNG))

	controlData := buf.Bytes()

	// function
	paramString := `sharpen_100`
	params, err := NewExtendedParameters(paramString)
	assert.NoError(err)
	data, err := ioutil.ReadFile("sample.png")
	assert.NoError(err)
	transform, err := New(data)
	assert.NoError(err)
	transform.Transform(params)
	transformeData, err := transform.Encode(imaging.PNG)

	assert.True(bytes.Equal(controlData, transformeData))
}
func Test_Transform_Rotate(t *testing.T) {
	assert := assert.New(t)
	//control
	f, err := os.Open("sample.png")
	assert.NoError(err)
	controlImage, err := imaging.Decode(f)
	assert.NoError(err)
	controlImage = imaging.Rotate(controlImage, 100, color.Black)
	assert.NoError(err)
	buf := bytes.Buffer{}
	assert.NoError(imaging.Encode(&buf, controlImage, imaging.PNG))

	controlData := buf.Bytes()

	// function
	paramString := `rotate_100`
	params, err := NewExtendedParameters(paramString)
	assert.NoError(err)
	data, err := ioutil.ReadFile("sample.png")
	assert.NoError(err)
	transform, err := New(data)
	assert.NoError(err)
	transform.Transform(params)
	transformeData, err := transform.Encode(imaging.PNG)

	assert.True(bytes.Equal(controlData, transformeData))
}
func Test_Transform_BlurAndGamma(t *testing.T) {
	assert := assert.New(t)
	//control
	f, err := os.Open("sample.png")
	assert.NoError(err)
	controlImage, err := imaging.Decode(f)
	assert.NoError(err)
	controlImage = imaging.AdjustContrast(controlImage, 100)
	controlImage = imaging.Resize(controlImage, 100, 100, imaging.Lanczos)
	controlImage = imaging.Blur(controlImage, 100)
	controlImage = imaging.AdjustGamma(controlImage, 0.5)

	assert.NoError(err)
	buf := bytes.Buffer{}
	assert.NoError(imaging.Encode(&buf, controlImage, imaging.PNG))

	controlData := buf.Bytes()

	// function
	paramString := `contrast_100,width_100,blur_100,gamma_0.5,height_100`
	params, err := NewExtendedParameters(paramString)
	assert.NoError(err)
	data, err := ioutil.ReadFile("sample.png")
	assert.NoError(err)
	transform, err := New(data)
	assert.NoError(err)
	transform.Transform(params)
	transformeData, err := transform.Encode(imaging.PNG)

	assert.True(bytes.Equal(controlData, transformeData))
}
