package test_suite

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface"
	"github.com/go-xorm/xorm"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"progimage.com/app/models"
)

type downer struct {
	s3manageriface.DownloaderAPI
	bucket string
	active bool
}

func (d *downer) Download(a io.WriterAt, b *s3.GetObjectInput, c ...func(*s3manager.Downloader)) (int64, error) {
	if d.active {
		fmt.Println("download from directory: ", d.bucket)
		key := d.bucket + "/" + aws.StringValue(b.Key)
		f, err := ioutil.ReadFile(key)
		if err != nil {
			fmt.Println("invalid file", err)
			return 0, nil
		}
		n, err := a.WriteAt(f, 0)
		return int64(n), err

	}
	return 100, nil
}

func (d *downer) DownloadWithContext(aws.Context, io.WriterAt, *s3.GetObjectInput, ...func(*s3manager.Downloader)) (int64, error) {
	return 0, nil
}

type upper struct {
	s3manageriface.UploaderAPI
	bucket string
	active bool
}

func (u upper) Upload(input *s3manager.UploadInput, options ...func(*s3manager.Uploader)) (*s3manager.UploadOutput, error) {
	if u.active {
		fmt.Println("do uploads to directory ", u.bucket)
		body := input.Body
		// create file
		file, err := os.Create(u.bucket + "/" + aws.StringValue(input.Key))
		if err != nil {
			return nil, err
		}
		defer file.Close()
		if _, err := io.Copy(file, body); err != nil {
			return nil, err
		}

	}
	return &s3manager.UploadOutput{}, nil
}

func (m *upper) UploadWithContext(aws.Context, *s3manager.UploadInput, ...func(*s3manager.Uploader)) (*s3manager.UploadOutput, error) {
	return nil, nil
}

type ServiceSuite struct {
	A              *assert.Assertions
	tmpDir         string
	Uploader       *upper
	Downloader     *downer
	DB             *xorm.Engine
	Ctx            context.Context
	CopySamplePath string
	CdnURL         string
	bucket         string
}

func New(t *testing.T, db *xorm.Engine) *ServiceSuite {
	uuid := uuid.Must(uuid.NewV4()).String()

	assert := assert.New(t)
	createTmpDir(assert, uuid)
	suite := &ServiceSuite{
		Ctx:            context.Background(),
		A:              assert,
		tmpDir:         uuid,
		Uploader:       &upper{},
		Downloader:     &downer{},
		DB:             db,
		CopySamplePath: copySample(assert, "sample.jpg", uuid+"/sample.jpg"),
	}

	return suite
}
func (s *ServiceSuite) HookCdn() *ServiceSuite {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		f, err := ioutil.ReadFile("sample.jpg")
		s.A.NoError(err)
		w.Write([]byte(f))
	}))
	s.CdnURL = ts.URL
	return s
}

func (s *ServiceSuite) WithActiveUploader() *ServiceSuite {
	s.bucket = uuid.Must(uuid.NewV4()).String()
	s.Uploader.active, s.Downloader.active = true, true
	s.Uploader.bucket, s.Downloader.bucket = s.bucket, s.bucket
	s.A.NoError(os.Mkdir(s.bucket, 0750))
	return s
}

func (s *ServiceSuite) ValidateFileExists(file *models.FileInfo) {
	if file != nil {
		var fi models.FileInfo
		yes, _ := s.DB.SQL(
			fmt.Sprintf("select * from %s where id =? and uploaded=?", models.FileInfoTableName), file.ID, true,
		).Get(&fi)
		s.A.True(yes)

		// check for file if it exists
		if s.bucket != "" {
			_, err := os.Stat(s.bucket + "/" + file.ID)
			s.A.NoError(err)
		}

	}
}

func (s *ServiceSuite) Cleanup(files ...*models.FileInfo) {
	deleteTmpDir(s.A, s.tmpDir)
	for _, file := range files {
		if file != nil {
			deleteFileInfoEntry(s.A, s.DB, file.ID)
		}
	}
	if s.bucket != "" {
		s.A.NoError(os.RemoveAll(s.bucket))
	}

}

func createTmpDir(a *assert.Assertions, path string) {

	a.NoError(os.Mkdir(path, 0750))

}

func deleteTmpDir(a *assert.Assertions, path string) {
	a.NoError(os.RemoveAll(path))

}
func deleteFileInfoEntry(a *assert.Assertions, db *xorm.Engine, id string) {
	_, err := db.Table(models.FileInfoTableName).Where("id=?", id).Delete(&models.FileInfo{})
	a.NoError(err)
}

func copySample(a *assert.Assertions, src, dst string) string {
	in, err := os.Open(src)
	a.NoError(err)
	defer in.Close()

	out, err := os.Create(dst)
	a.NoError(err)
	defer out.Close()

	_, err = io.Copy(out, in)
	a.NoError(err)
	a.NoError(out.Close())
	return dst
}
