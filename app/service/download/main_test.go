package download_test

import (
	"fmt"
	"image"
	"io"
	"mime"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/disintegration/imaging"
	"github.com/go-kit/kit/log"
	"github.com/go-xorm/xorm"
	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
	"progimage.com/app/models"
	"progimage.com/app/service/download"
	"progimage.com/app/service/test_suite"
	"progimage.com/utils/db"
	"progimage.com/utils/s3"
	"progimage.com/utils/transaction"
)

func TestMain(t *testing.T) {
	suite := test_suite.New(t, engine).WithActiveUploader()

	// prepare an entry and a file to be downloaded
	key := uuid.Must(uuid.NewV4()).String()
	fi := &models.FileInfo{
		ID:       key,
		Uploaded: true,
	}
	f, err := os.Open("sample.jpg")
	suite.A.NoError(err)
	defer f.Close()
	suite.A.NoError(s3.UploadBuffer(suite.Uploader, "bucket", key, f))
	_, err = engine.Table(models.FileInfoTableName).Insert(fi)
	suite.A.NoError(err)
	//prep end
	defer t.Cleanup(func() {
		suite.Cleanup(fi)
	})

	router := mux.NewRouter()
	download.NewService(engine, suite.Downloader).Route(router, logger)
	testServer := httptest.NewServer(router)

	// get Valid ID
	downloadValidFile := &fileDownloader{
		url: testServer.URL + "/download/" + key + ".png",
	}
	suite.A.NoError(transaction.Get(downloadValidFile))
	suite.A.Equal(downloadValidFile.format, imaging.PNG)
	suite.A.Equal(downloadValidFile.statusCode, 200)

	downloadFileInvalidExt := &fileDownloader{
		url: testServer.URL + "/download/" + key + ".abc",
	}
	suite.A.Error(transaction.Get(downloadFileInvalidExt))
	suite.A.Equal(downloadFileInvalidExt.statusCode, http.StatusBadRequest)

	downloadFileInvalidID := &fileDownloader{
		url: testServer.URL + "/download/" + uuid.Must(uuid.NewV4()).String() + ".png",
	}
	suite.A.Error(transaction.Get(downloadFileInvalidID))
	suite.A.Equal(downloadFileInvalidID.statusCode, http.StatusNotFound)

	downloadWithTransform := &fileDownloader{
		url: testServer.URL + "/download/crop_c,width_10,height_10/" + key + ".png",
	}
	suite.A.NoError(transaction.Get(downloadWithTransform))
	fmt.Println("format", downloadWithTransform.format)
	suite.A.Equal(downloadWithTransform.format, imaging.PNG)
	suite.A.Equal(downloadWithTransform.statusCode, 200)

	downloadWithInvalidTransform := &fileDownloader{
		url: testServer.URL + "/download/crop___c/" + key + ".png",
	}
	suite.A.Error(transaction.Get(downloadWithInvalidTransform))
	suite.A.Equal(downloadWithInvalidTransform.statusCode, http.StatusBadRequest)

}

type fileDownloader struct {
	url        string
	image      image.Image
	format     imaging.Format
	statusCode int
}

func (fd fileDownloader) URL() string {
	return fd.url
}
func (fd *fileDownloader) StatusCode(i int) {
	fd.statusCode = i
}

func (fd *fileDownloader) ParseBody(rc io.ReadCloser) error {
	image, err := imaging.Decode(rc)
	if err != nil {
		return err
	}
	fd.image = image

	return nil
}
func (fd *fileDownloader) ParseHeader(resp http.Header) {
	extensions, _ := mime.ExtensionsByType(resp.Get("Content-Type"))
	for _, ext := range extensions {
		if format, err := imaging.FormatFromExtension(ext); err == nil {
			fd.format = format
		}
	}
}

const (
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbName   = "progimage"
)

var (
	host      = "localhost"
	tmpDir    string
	minioHost = "localhost"
	minioPort = "9000"
)

//AWS
var (
	Region          = "us-west-1"
	AccessKeyId     = "minio"
	SecretAccessKey = "minio123"
	s3Sesh          *session.Session
	engine          *xorm.Engine
	logger          = log.NewLogfmtLogger(os.Stderr)
)

func init() {
	e, err := db.New(host, user, password, dbName, port)
	if err != nil {
		panic(err)
	}
	engine = e
}
