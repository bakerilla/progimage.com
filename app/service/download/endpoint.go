package download

import (
	"context"

	"github.com/disintegration/imaging"
	"github.com/go-kit/kit/endpoint"
	"progimage.com/app/apperr"
	"progimage.com/app/models"
	"progimage.com/app/transform"
)

type DownloadInfo struct {
	params transform.ExtendedParameters
	ID     string
	Format imaging.Format
}
type DownloadResponse struct {
	Data []byte
}

func DownloadFileEndpoint(svc *service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		var (
			req      = request.(DownloadInfo)
			fileinfo models.FileInfo
		)

		ok, err := svc.db.Where("id = ?", req.ID).And("uploaded = true").Limit(10, 0).Get(&fileinfo)
		if err != nil {
			return nil, apperr.New(apperr.ErrCodeNotFound, err.Error())
		}
		if !ok {
			return nil, apperr.New(apperr.ErrCodeNotFound, "resource not found")
		}

		return svc.DownloadAndTransformFile(ctx, &req)

	}
}
