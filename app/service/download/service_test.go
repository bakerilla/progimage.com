package download_test

import (
	"fmt"
	"mime"
	"net/http"
	"os"
	"testing"

	"github.com/disintegration/imaging"
	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
	"progimage.com/app/service/download"
	"progimage.com/app/service/test_suite"
	"progimage.com/utils/s3"
)

func Test_Service_TestRoute(t *testing.T) {
	suite := test_suite.New(t, engine)
	defer t.Cleanup(func() {
		suite.Cleanup(nil)
	})

	router := mux.NewRouter()
	download.NewService(engine, suite.Downloader).Route(router, logger)

	// validate endpoints
	endpoints := map[string]bool{
		"GET:::/download/{id}.{ext}":                   true,
		"HEAD:::/download/{id}.{ext}":                  true,
		"GET:::/download/{transformation}/{id}.{ext}":  true,
		"HEAD:::/download/{transformation}/{id}.{ext}": true,
	}

	routeCount := 0
	router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		tpl, _ := route.GetPathTemplate()
		met, _ := route.GetMethods()
		fmt.Println("x", tpl, met)
		for _, m := range met {
			key := fmt.Sprintf("%s:::%s", m, tpl)
			if _, ok := endpoints[key]; ok {
				routeCount++
			}
		}
		return nil
	})
	suite.A.Equal(len(endpoints), routeCount)

}

func Test_Service_DownloadAndTransformFile(t *testing.T) {
	suite := test_suite.New(t, engine).WithActiveUploader()
	defer t.Cleanup(func() {
		suite.Cleanup(nil)
	})

	// prepare a file to be downloaded
	key := uuid.Must(uuid.NewV4()).String()

	f, err := os.Open("sample.jpg")
	suite.A.NoError(err)
	defer f.Close()
	suite.A.NoError(s3.UploadBuffer(suite.Uploader, "bucket", key, f))

	service := download.NewService(engine, suite.Downloader)

	data, err := service.DownloadAndTransformFile(suite.Ctx, &download.DownloadInfo{
		ID:     key,
		Format: imaging.PNG,
	})
	suite.A.NoError(err)
	ct := http.DetectContentType(data.Data) //<- mime
	fmt.Println("ct", ct)
	extensions, _ := mime.ExtensionsByType(ct)
	suite.A.NotNil(extensions)
	for _, e := range extensions {
		format, err := imaging.FormatFromExtension(e)
		if err == nil {
			suite.A.Equal(imaging.PNG, format)
		}

	}

}
