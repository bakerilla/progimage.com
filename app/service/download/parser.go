package download

import (
	"context"
	"net/http"

	"github.com/disintegration/imaging"
	"github.com/gorilla/mux"
	"progimage.com/app/apperr"
	"progimage.com/app/transform"
)

func decodeDownloadRequest(ctx context.Context, req *http.Request) (interface{}, error) {
	var (
		v   = mux.Vars(req)
		id  = v["id"]
		ext = v["ext"]
	)
	params, err := transform.NewExtendedParameters(v["transformation"])
	if err != nil {
		return nil, err
	}
	format, err := imaging.FormatFromExtension(ext)
	if err != nil {
		return nil, apperr.New(apperr.ErrCodeBadRequest, err.Error())
	}

	return DownloadInfo{
		ID:     id,
		params: *params,
		Format: format,
	}, nil
}
