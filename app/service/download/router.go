package download

import (
	"context"
	"net/http"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	s "progimage.com/app/service"
	"progimage.com/app/service/responder"
)

func (svc *service) newRouter(mainRouter *mux.Router, logger log.Logger) *mux.Router {

	// uuid regex: [0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}

	// /download/{id:[0-9]+} -> without transformation

	// /download/{transformation}/{id:[0-9]+} -> with transformation

	options := []httptransport.ServerOption{
		httptransport.ServerErrorLogger(logger),
		httptransport.ServerErrorEncoder(responder.EncodeErrorResponse),
		httptransport.ServerBefore(s.LogWith(logger, "event", "http").Incoming),
	}
	var downloadFile endpoint.Endpoint
	{
		downloadFile = DownloadFileEndpoint(svc)
	}
	downloadFileHandler := httptransport.NewServer(
		downloadFile,          //use the endpoint
		decodeDownloadRequest, //converts the parameters received via the request body into the struct expected by the endpoint
		ImageReturner,         //converts the struct returned by the endpoint to a json response
		options...,
	)

	// r := mainRouter.PathPrefix(svc.alias).Subrouter() //I'm using Gorilla Mux, but it could be any other library, or even the stdlib
	r := mainRouter.PathPrefix("/download").Subrouter()
	r.Methods(http.MethodGet, http.MethodHead).Path("/{id}.{ext}").Handler(downloadFileHandler)
	r.Methods(http.MethodGet, http.MethodHead).Path("/{transformation}/{id}.{ext}").Handler(downloadFileHandler)

	return mainRouter
}

func ImageReturner(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	resp := response.(*DownloadResponse)
	w.Write(resp.Data)
	return nil
}
