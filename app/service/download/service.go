package download

import (
	"context"

	// "github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface"
	"github.com/go-kit/kit/log"
	"github.com/go-xorm/xorm"
	"github.com/gorilla/mux"
	s "progimage.com/app/service"
	"progimage.com/app/transform"
	"progimage.com/utils/s3"
)

type service struct {
	db         xorm.Interface
	downloader s3manageriface.DownloaderAPI
}

type download interface {
	s.Service
	DownloadAndTransformFile(ctx context.Context, info *DownloadInfo) (*DownloadResponse, error)
}

type fileData struct {
}

func NewService(db xorm.Interface, downloader s3manageriface.DownloaderAPI) download {
	return newService(db, downloader)
}
func newService(db xorm.Interface, downloader s3manageriface.DownloaderAPI) download {
	return &service{db: db, downloader: downloader}
}

func (svc *service) Route(mainRouter *mux.Router, log log.Logger) {
	svc.newRouter(mainRouter, log)

}

func (svc *service) DownloadAndTransformFile(ctx context.Context, info *DownloadInfo) (*DownloadResponse, error) {
	data, err := s3.Download(svc.downloader, s.ServiceBucket, info.ID)
	if err != nil {
		return nil, err
	}
	transformer, err := transform.New(data)
	if err != nil {
		return nil, err
	}

	transformer.Transform(&info.params)
	b, err := transformer.Encode(info.Format)
	if err != nil {
		return nil, err
	}

	return &DownloadResponse{
		Data: b,
	}, nil
}
