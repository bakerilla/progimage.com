package batch

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	"progimage.com/app/apperr"
	"progimage.com/app/models"
)

// BatchStatusEndpoint to fetch from s3 and send to user
func (svc *service) BatchStatusEndpoint() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		var (
			id    = request.(string)
			batch models.BatchTransformation
		)

		found, err := svc.db.Table(models.BatchTransformationTableName).Where("id=?", id).Get(&batch)
		if err != nil || !found {
			return nil, apperr.New(apperr.ErrCodeNotFound, "not found")
		}

		return batch, nil

	}
}
