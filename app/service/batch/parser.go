package batch

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/disintegration/imaging"
	"github.com/gorilla/mux"
	"progimage.com/app/apperr"
	"progimage.com/app/models"
	"progimage.com/app/transform"
)

type batchTransformationRequest struct {
	Files          []string `json:"files"`
	Transformation string   `json:"transformation,omitempty"`
	parameters     transform.ExtendedParameters
	extension      string
}

type batchTransformationResponse struct {
	batchTransformationRequest
	models.BatchTransformation
}

func decodeNewBatchRequest(ctx context.Context, req *http.Request) (interface{}, error) {
	var (
		v       = mux.Vars(req)
		ext     = v["extension"]
		request batchTransformationRequest
	)
	if err := json.NewDecoder(req.Body).Decode(&request); err != nil {
		return nil, err
	}
	if len(request.Files) == 0 {
		return nil, apperr.New(apperr.ErrCodeBadRequest, "please provide a list of IDs")
	}
	_, err := imaging.FormatFromExtension(ext)
	if err != nil {
		return nil, apperr.New(apperr.ErrCodeBadRequest, err.Error())
	}
	params, err := transform.NewExtendedParameters(request.Transformation)
	if err != nil {
		return nil, apperr.New(apperr.ErrCodeBadRequest, err.Error())
	}
	request.extension = ext
	request.parameters = *params

	return request, nil
}
func decodeDownloadRequest(ctx context.Context, req *http.Request) (interface{}, error) {
	return mux.Vars(req)["id"], nil
}
func decodeBatchStatusRequest(ctx context.Context, req *http.Request) (interface{}, error) {
	var (
		v  = mux.Vars(req)
		id = v["id"]
	)
	return id, nil
}
