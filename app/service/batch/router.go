package batch

import (
	"context"
	"net/http"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"progimage.com/app/service/responder"
)

func (svc *service) newRouter(mainRouter *mux.Router, logger log.Logger) *mux.Router {
	options := []httptransport.ServerOption{
		httptransport.ServerErrorLogger(logger),
		httptransport.ServerErrorEncoder(responder.EncodeErrorResponse),
		// httptransport.ServerBefore(ctxset.PopulateHeaders),
	}
	var (
		downloadBatch,
		createBatch,
		batchStatus endpoint.Endpoint
	)
	{
		downloadBatch = svc.DownloadBatchEndpoint()
		createBatch = svc.CreateBatchEndpoint()
		batchStatus = svc.BatchStatusEndpoint()
	}
	downloadBatchHandler := httptransport.NewServer(
		downloadBatch,         //use the endpoint
		decodeDownloadRequest, //converts the parameters received via the request body into the struct expected by the endpoint
		ImageReturner,         //converts the struct returned by the endpoint to a json response
		options...,
	)
	createBatchHandler := httptransport.NewServer(
		createBatch,                       //use the endpoint
		decodeNewBatchRequest,             //converts the parameters received via the request body into the struct expected by the endpoint
		responder.EncodeJSONResponse(nil), //converts the struct returned by the endpoint to a json response
		options...,
	)
	batchStatusHandler := httptransport.NewServer(
		batchStatus,                       //use the endpoint
		decodeBatchStatusRequest,          //converts the parameters received via the request body into the struct expected by the endpoint
		responder.EncodeJSONResponse(nil), //converts the struct returned by the endpoint to a json response
		options...,
	)

	// r := mainRouter.PathPrefix(svc.alias).Subrouter() //I'm using Gorilla Mux, but it could be any other library, or even the stdlib
	r := mainRouter.PathPrefix("/batch").Subrouter()
	r.Methods(http.MethodPost).Path("/{extension}").Handler(createBatchHandler)            // request: {[]id, transformation}
	r.Methods(http.MethodGet, http.MethodHead).Path("/{id}").Handler(downloadBatchHandler) //downloadhandler
	r.Methods(http.MethodGet, http.MethodHead).Path("/{id}/status").Handler(batchStatusHandler)

	return mainRouter
}

func ImageReturner(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	resp := response.(*DownloadResponse)
	w.Write(resp.Data)
	return nil
}
