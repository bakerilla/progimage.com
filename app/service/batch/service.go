package batch

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface"
	"github.com/go-kit/kit/log"
	"github.com/go-xorm/xorm"
	"github.com/gorilla/mux"
	s "progimage.com/app/service"
)

type service struct {
	db         *xorm.Engine
	downloader s3manageriface.DownloaderAPI
}

type fileData struct {
}

func NewService(db *xorm.Engine, downloader s3manageriface.DownloaderAPI) s.Service {
	return newService(db, downloader)
}
func newService(db *xorm.Engine, downloader s3manageriface.DownloaderAPI) *service {
	return &service{db: db, downloader: downloader}
}

func (svc *service) Route(mainRouter *mux.Router, log log.Logger) {
	svc.newRouter(mainRouter, log)

}

func (svc *service) DownloadZippedFile(ctx context.Context, id string) (*DownloadResponse, error) {
	buff := &aws.WriteAtBuffer{}
	input := &s3.GetObjectInput{
		Bucket: aws.String(s.BatchBucket),
		Key:    aws.String(id + ".zip"),
	}
	if _, err := svc.downloader.Download(buff, input); err != nil {
		return nil, err
	}
	data := buff.Bytes()

	return &DownloadResponse{
		Data: data,
	}, nil
}
