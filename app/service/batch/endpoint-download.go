package batch

import (
	"context"

	"github.com/disintegration/imaging"
	"github.com/go-kit/kit/endpoint"
	"progimage.com/app/apperr"
	"progimage.com/app/models"
	"progimage.com/app/transform"
)

type downloadInfo struct {
	params transform.Parameters
	id     string
	format imaging.Format
}
type DownloadResponse struct {
	Data []byte
}

// DownloadBatchEndpoint to fetch from s3 and send to user
func (svc *service) DownloadBatchEndpoint() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		var (
			id       = request.(string)
			fileinfo models.FileInfo
		)

		ok, err := svc.db.Where("id = ?", id).And("uploaded = true").Limit(10, 0).Get(&fileinfo)
		if err != nil {
			return nil, apperr.New(apperr.ErrCodeNotFound, err.Error())
		}
		if !ok {
			return nil, apperr.New(apperr.ErrCodeNotFound, "resource not found")
		}

		return svc.DownloadZippedFile(ctx, id)

	}
}
