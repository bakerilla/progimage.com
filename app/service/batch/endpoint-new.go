package batch

import (
	"context"
	"fmt"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/gofrs/uuid"
	"progimage.com/app/apperr"
	"progimage.com/app/models"
	"xorm.io/builder"
)

// DownloadBatchEndpoint to fetch from s3 and send to user
func (svc *service) CreateBatchEndpoint() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		var (
			req      = request.(batchTransformationRequest)
			fileinfo []models.FileInfo
			iff      []interface{}
			batch    = &models.BatchTransformation{
				ID:          uuid.Must(uuid.NewV4()).String(),
				Parameters:  req.parameters,
				Extension:   req.extension,
				FileIDs:     req.Files,
				DateCreated: time.Now().UTC(),
				Bstatus:     "pending",
			}
		)

		// 1: find and count
		for _, v := range req.Files {
			iff = append(iff, v)
		}
		count, err := svc.db.Table(models.FileInfoTableName).Where(builder.In("id", iff...)).And("uploaded=?", true).FindAndCount(&fileinfo)
		if err != nil {
			fmt.Println("err select", err)
			return nil, apperr.New(apperr.ErrCodeBadRequest, "invalid file ids provided")
		}
		if int(count) != len(req.Files) {
			return nil, apperr.New(apperr.ErrCodeBadRequest, "invalid file ids provided")
		}
		if _, err := svc.db.Table(models.BatchTransformationTableName).Insert(batch); err != nil {
			return nil, err
		}

		// insert
		//done
		fmt.Println("req", req)
		return &batchTransformationResponse{
			batchTransformationRequest: req,
			BatchTransformation:        *batch,
		}, nil

	}
}
