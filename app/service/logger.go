package service

import (
	"context"
	"net/http"

	"github.com/go-kit/kit/log"
)

// type RequestFunc func(context.Context, *http.Request) context.Context

type ll struct {
	logger log.Logger
}

func LogWith(logger log.Logger, args ...interface{}) *ll {
	return &ll{
		logger: log.With(logger, args...),
	}

}

func (l *ll) Incoming(ctx context.Context, req *http.Request) context.Context {
	l.logger.Log(
		"method", req.Method,
		"content-length", req.ContentLength,
		"uri", req.RequestURI,
	)
	return ctx
}
