package example

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-kit/kit/log"
	"github.com/gofrs/uuid"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	nr "github.com/newrelic/go-agent/v3/newrelic"
	s "progimage.com/app/service"
)

type service struct {
	db  *sqlx.DB
	app *nr.Application
}

type example interface {
	s.ServiceWithMetrics
}

func New(app *nr.Application, db *sqlx.DB) example {
	return &service{
		app: app,
		db:  db,
	}
}

func (svc *service) Route(mainRouter *mux.Router, log log.Logger) {
	svc.newRouter(mainRouter, log)
}

func (svc *service) InitMetrics(ctx context.Context, req *http.Request) context.Context {
	transaction := svc.app.StartTransaction(req.RequestURI)
	for k, v := range map[s.ContextKey]interface{}{
		"seed":                    uuid.Must(uuid.NewV4()).String(),
		s.ContextKeyNRTransaction: transaction,
	} {
		ctx = context.WithValue(ctx, k, v)
	}
	return ctx
}
func (svc *service) Flush(ctx context.Context, rw http.ResponseWriter) context.Context {
	fmt.Println("flush")

	tmp := ctx.Value(s.ContextKeyNRTransaction)
	tx, ok := tmp.(*nr.Transaction)
	if !ok {
		fmt.Println("tx is nul")
		return ctx
	}
	tx.End()

	return ctx
}
