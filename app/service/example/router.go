package example

import (
	"context"
	"net/http"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	s "progimage.com/app/service"
	"progimage.com/app/service/responder"
)

func (svc *service) newRouter(mainRouter *mux.Router, logger log.Logger) *mux.Router {

	options := []httptransport.ServerOption{
		httptransport.ServerErrorLogger(logger),
		httptransport.ServerErrorEncoder(responder.EncodeErrorResponse),
		httptransport.ServerBefore(svc.InitMetrics, s.LogWith(logger, "event", "http").Incoming),
		httptransport.ServerAfter(svc.Flush),
		// httptransport.ServerBefore(ctxset.PopulateHeaders),
	}
	var exampleEndpt endpoint.Endpoint
	{
		exampleEndpt = ExampleEndpoint(svc)
	}
	exampleHandler := httptransport.NewServer(
		exampleEndpt,   //use the endpoint
		exampleDecoder, //converts the parameters received via the request body into the struct expected by the endpoint
		responder.EncodeJSONResponse(statusCodeHook), //converts the struct returned by the endpoint to a json response
		options...,
	)

	// r := mainRouter.PathPrefix(svc.alias).Subrouter() //I'm using Gorilla Mux, but it could be any other library, or even the stdlib
	mainRouter.Methods("GET").Path("/mamamia").Handler(exampleHandler)
	return mainRouter
}

func statusCodeHook(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(201)

	return nil
}
