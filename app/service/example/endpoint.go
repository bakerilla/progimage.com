package example

import (
	"context"

	"github.com/go-kit/kit/endpoint"
)

func ExampleEndpoint(svc example) endpoint.Endpoint {

	return func(ctx context.Context, req interface{}) (interface{}, error) {

		return nil, nil
	}
}
