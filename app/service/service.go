package service

import (
	"context"
	"fmt"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/go-kit/kit/log"

	"github.com/gorilla/mux"
)

var (
	ServiceBucket = "image"
	BatchBucket   = "batch"
)

type Service interface {
	Route(*mux.Router, log.Logger)
}

type ServiceWithMetrics interface {
	Service
	InitMetrics(context.Context, *http.Request) context.Context
	Flush(context.Context, http.ResponseWriter) context.Context
}

func InitializeBucket(sesh *session.Session) error {
	s3Client := s3.New(sesh)
	if err := createBucket(s3Client, ServiceBucket); err != nil {
		return err
	}
	if err := createBucket(s3Client, BatchBucket); err != nil {
		return err
	}
	return nil
}

func createBucket(s3Client *s3.S3, name string) error {
	cparams := &s3.CreateBucketInput{
		Bucket: aws.String(name), // Required
	}

	if _, err := s3Client.CreateBucket(cparams); err != nil {
		if awsErr, ok := err.(awserr.Error); ok {
			if awsErr.Code() == s3.ErrCodeBucketAlreadyExists {
				return nil
			}
			return err
		}
		fmt.Println(err.Error())
		return err
	}
	return nil
}
