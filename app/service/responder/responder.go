package responder

import (
	"context"
	"encoding/json"
	"net/http"

	httptransport "github.com/go-kit/kit/transport/http"
	"progimage.com/app/apperr"
)

const (
	responderContextKey        = "responder-"
	ResponderContextStatusCode = responderContextKey + "statusCode"
)

func EncodeErrorResponse(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(codeFrom(err))
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}

func codeFrom(err error) int {
	if tmp, ok := err.(*apperr.Error); ok {
		return tmp.HTTPStatusCode()
	}
	return http.StatusInternalServerError
}

func EncodeJSONResponse(enc httptransport.EncodeResponseFunc) httptransport.EncodeResponseFunc {
	return func(ctx context.Context, w http.ResponseWriter, response interface{}) error {
		if enc != nil {
			enc(ctx, w, response)
		}
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		return json.NewEncoder(w).Encode(response)
	}

}
