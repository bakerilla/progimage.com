package service

const (
	contextKeyPrefix        = ContextKey("ck-")
	ContextKeyNRTransaction = contextKeyPrefix + "nr-tx"
)

type ContextKey string
