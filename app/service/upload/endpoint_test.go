package upload_test

import (
	"testing"

	"progimage.com/app/models"
	"progimage.com/app/service/test_suite"
	"progimage.com/app/service/upload"
)

func Test_UploadEndpoint(t *testing.T) {
	var (
		fileInfo *models.FileInfo
		err      error
		ok       bool
	)

	suite := test_suite.New(t, engine)

	defer t.Cleanup(func() {
		suite.Cleanup(fileInfo)
	})

	svc := upload.NewService(suite.DB, suite.Uploader)

	endpoint := upload.UploadFileEndpoint(svc)
	resp, err := endpoint(suite.Ctx, &upload.Localfile{
		StoredPath: suite.CopySamplePath,
	})
	fileInfo, ok = resp.(*models.FileInfo)
	suite.A.True(ok)
	suite.A.NoError(err)
	suite.ValidateFileExists(fileInfo)

}
func Test_FetchEndpoint(t *testing.T) {
	var (
		fileInfo *models.FileInfo
		err      error
		ok       bool
	)

	suite := test_suite.New(t, engine).HookCdn()

	defer t.Cleanup(func() {
		suite.Cleanup(fileInfo)
	})

	svc := upload.NewService(suite.DB, suite.Uploader)

	endpoint := upload.FetchFileEndpoint(svc)
	resp, err := endpoint(suite.Ctx, &upload.FetchUrl{
		Url: suite.CdnURL,
	})
	fileInfo, ok = resp.(*models.FileInfo)
	suite.A.True(ok)
	suite.A.NoError(err)
	suite.ValidateFileExists(fileInfo)

}
