package upload_test

// /service.go, endpoint.go unit testing
import (
	"fmt"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/go-kit/kit/log"
	"github.com/go-xorm/xorm"
	"github.com/gorilla/mux"
	"progimage.com/app/models"
	"progimage.com/app/service/test_suite"
	"progimage.com/app/service/upload"
	"progimage.com/utils/db"
)

const (
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbName   = "progimage"
)

var (
	host      = "localhost"
	tmpDir    string
	minioHost = "localhost"
	minioPort = "9000"
)

//AWS
var (
	Region          = "us-west-1"
	AccessKeyId     = "minio"
	SecretAccessKey = "minio123"
	s3Sesh          *session.Session
	engine          *xorm.Engine
	logger          = log.NewLogfmtLogger(os.Stderr)
)

func init() {
	e, err := db.New(host, user, password, dbName, port)
	if err != nil {
		panic(err)
	}
	engine = e

}
func Test_Service_Route(t *testing.T) {
	// setup suite
	var (
		fileInfo *models.FileInfo
		// err      error
		r = mux.NewRouter()
	)

	suite := test_suite.New(t, engine)

	defer t.Cleanup(func() {
		suite.Cleanup(fileInfo)
	})

	// validate endpoints
	endpoints := map[string]bool{
		"POST:::/upload": true,
		"GET:::/fetch":   true,
		"HEAD:::/fetch":  true,
	}
	upload.NewService(suite.DB, suite.Uploader).Route(r, logger)
	routeCount := 0
	r.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		tpl, _ := route.GetPathTemplate()
		met, _ := route.GetMethods()
		for _, m := range met {
			key := fmt.Sprintf("%s:::%s", m, tpl)
			if _, ok := endpoints[key]; ok {
				routeCount++
			}
		}

		return nil
	})
	suite.A.Equal(len(endpoints), routeCount)

}

func Test_Service_UploadFile(t *testing.T) {

	// setup suite
	var (
		fileInfo *models.FileInfo
		err      error
	)

	suite := test_suite.New(t, engine)

	defer t.Cleanup(func() {
		suite.Cleanup(fileInfo)
	})

	svc := upload.NewService(suite.DB, suite.Uploader)

	fileInfo, err = svc.UploadFile(suite.Ctx, &upload.Localfile{
		StoredPath: suite.CopySamplePath,
	})
	suite.A.NoError(err)
	suite.ValidateFileExists(fileInfo)

	// negative case, file is gone
	_, err = svc.UploadFile(suite.Ctx, &upload.Localfile{})
	suite.A.Error(err)

}

func Test_Service_FetchFile(t *testing.T) {

	var (
		fileInfo *models.FileInfo
		err      error
	)
	// setup
	suite := test_suite.New(t, engine).HookCdn()

	defer t.Cleanup(func() {
		suite.Cleanup(fileInfo)
	})

	svc := upload.NewService(suite.DB, suite.Uploader)
	fileInfo, err = svc.FetchFile(suite.Ctx, &upload.FetchUrl{
		Url: suite.CdnURL,
	})

	suite.A.NoError(err)
	suite.ValidateFileExists(fileInfo)

	// invalid url
	_, err = svc.FetchFile(suite.Ctx, &upload.FetchUrl{})
	suite.A.Error(err)

}
