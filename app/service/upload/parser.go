package upload

import (
	"context"
	"fmt"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"progimage.com/app/apperr"
	tx "progimage.com/utils/transaction"
)

var TmpDir = "tmp-images"

type transaction struct {
	tx.Transaction
	isImage    bool
	extensions []string
}

/*
// unused for now, might extend another function for this
func decodeMultipleFileUploadRequest(ctx context.Context, req *http.Request) (interface{}, error) {
	var info uploadInfo
	req.ParseMultipartForm(32 << 20) // limits file to 32mb

	data := req.MultipartForm

	//get the *fileheaders
	files := data.File["file"] // grab the filenames

	for i, mdata := range files {
		file, err := files[i].Open()
		name := files[i].Filename
		defer file.Close()
		out, err := ioutil.TempFile(TmpDir, "xxx-*.png")
		if err != nil {
			return nil, err
		}
		defer out.Close()
		if _, err := io.Copy(out, file); err != nil {
			return nil, err
		}

		out.Seek(0, 0)
		if _, _, err := image.Decode(out); err != nil {
			if err := os.Remove(out.Name()); err != nil {
				return nil, err
			}
			// clean everything
			for _, tmp := range info.Files {
				os.Remove(tmp.StoredPath)
			}

			return nil, apperr.New(apperr.ErrCodeBadRequest, fmt.Sprintf("invalid file format for file %s", name))
		}
		localFile := &Localfile{
			Metadata:     mdata,
			StoredPath:   out.Name(),
			OriginalName: name,
		}
		info.Files = append(info.Files, localFile)

	}
	return info, nil
}
*/

func decodeUploadRequest(ctx context.Context, req *http.Request) (interface{}, error) {
	// var info uploadInfo
	req.ParseMultipartForm(32 << 20) // limits file to 32mb

	file, header, err := req.FormFile("file")
	if err != nil {
		fmt.Println("error parsing", err)
		return nil, err
	}
	name := header.Filename
	defer file.Close()
	out, err := ioutil.TempFile(TmpDir, "xxx-*")
	if err != nil {
		return nil, err
	}
	defer out.Close()
	if _, err := io.Copy(out, file); err != nil {
		return nil, err
	}

	out.Seek(0, 0)
	if _, _, err := image.Decode(out); err != nil {
		if err := os.Remove(out.Name()); err != nil {
			return nil, err
		}

		return nil, apperr.New(apperr.ErrCodeBadRequest, fmt.Sprintf("invalid file format for file %s", name))
	}
	localFile := &Localfile{
		Metadata:     header,
		StoredPath:   out.Name(),
		OriginalName: name,
	}

	return localFile, nil
}

func decodeFetchFileRequest(ctx context.Context, req *http.Request) (interface{}, error) {

	var (
		v       = req.URL.Query()
		fetcher = &FetchUrl{
			Url: v.Get("url"),
		}
	)
	tx.Head(fetcher)
	if !fetcher.image() {
		return "", apperr.New(apperr.ErrCodeBadRequest, "url does not contain an image")
	}
	return fetcher, nil
}
