package upload

import (
	"context"
	"io"
	"io/ioutil"
	"mime"
	"mime/multipart"
	"net/http"
	"regexp"

	"github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface"
	"github.com/go-kit/kit/endpoint"
	"progimage.com/app/models"
	s "progimage.com/app/service"
	"progimage.com/utils/s3"
)

type uploadInfo struct {
	Files []*Localfile
	URL   *transaction
}

type Localfile struct {
	StoredPath   string
	OriginalName string
	Metadata     *multipart.FileHeader
}
type UplaodResponse struct {
	Successful []*models.FileInfo `json:"successful,omitempty"`
	Failed     []*models.FileInfo `json:"failed,omitempty"`
}

type FetchResponse models.FileInfo

func (t FetchUrl) URL() string {
	return t.Url
}
func (t *FetchUrl) StatusCode(i int) {
	return
}

type FetchUrl struct {
	Url        string
	isImage    bool
	extensions []string
	uuid       string
	uploader   s3manageriface.UploaderAPI
}

func (t *FetchUrl) image() bool {
	return t.isImage
}

// ParseHeader checks if this url contains an image
func (t *FetchUrl) ParseHeader(h http.Header) {
	var r = regexp.MustCompile(`image\/(?P<first>\w+)`)
	t.isImage = r.Match([]byte(h.Get("Content-Type")))
	t.extensions, _ = mime.ExtensionsByType(h.Get("Content-Type"))
}

func (t *FetchUrl) ParseBody(rc io.ReadCloser) error {
	data, err := ioutil.ReadAll(rc)
	if err != nil {
		return err
	}
	return s3.Upload(t.uploader, s.ServiceBucket, t.uuid, data)
	// fmt.Println("datsa", string(data))
}

func FetchFileEndpoint(svc upload) endpoint.Endpoint {
	// returns a json payload
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		var (
			fetcher = req.(*FetchUrl)
		)

		return svc.FetchFile(ctx, fetcher)
	}

}

func UploadFileEndpoint(svc upload) endpoint.Endpoint {
	// returns a json payload
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		var (
			request = req.(*Localfile)
		)
		return svc.UploadFile(ctx, request)
	}

}
