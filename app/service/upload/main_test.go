package upload_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"

	"github.com/go-kit/kit/log"
	"github.com/gorilla/mux"
	"progimage.com/app/models"
	"progimage.com/app/service/test_suite"
	"progimage.com/app/service/upload"
)

func Test_main(t *testing.T) {
	// test end 2 end
	router := mux.NewRouter()

	suite := test_suite.New(t, engine).HookCdn().WithActiveUploader()
	// setup TmpDir
	suite.A.NoError(os.Mkdir("test-tmpDir", 0755))
	upload.TmpDir = "test-tmpDir"

	defer t.Cleanup(func() {
		suite.Cleanup(nil)
		//cleanup TmpDir
		os.RemoveAll("test-tmpDir")
	})
	upload.NewService(suite.DB, suite.Uploader).Route(router, log.With(logger, "service", "uploader"))
	testServer := httptest.NewServer(router)

	res, _ := http.Head(testServer.URL + "/fetch?url=" + suite.CdnURL)
	suite.A.Equal(201, res.StatusCode)

	res, _ = http.Get(testServer.URL + "/fetch?url=https://google.com")
	suite.A.Equal(http.StatusBadRequest, res.StatusCode)

	uploadFunc := func(path string, url string) *http.Response {
		file, err := os.Open(path)
		suite.A.NoError(err)

		body := &bytes.Buffer{}
		writer := multipart.NewWriter(body)
		part, err := writer.CreateFormFile("file", filepath.Base(path))
		_, err = io.Copy(part, file)
		suite.A.NoError(err)

		err = writer.Close() //<-- important
		suite.A.NoError(err)

		request, err := http.NewRequest("POST", url, body)

		suite.A.NoError(err)
		request.Header.Add("Content-Type", writer.FormDataContentType())
		fmt.Println(request.Header)
		response, err := (&http.Client{}).Do(request)
		suite.A.NoError(err)
		return response

	}
	read := func(rc io.ReadCloser, i interface{}) {
		defer rc.Close()
		b, err := ioutil.ReadAll(rc)
		suite.A.NoError(err)
		suite.A.NoError(json.Unmarshal(b, i))

	}

	// uplooad from file (jpg)
	fi := models.FileInfo{}
	response := uploadFunc("sample.jpg", testServer.URL+"/upload")
	suite.A.Equal(http.StatusCreated, response.StatusCode)
	read(response.Body, &fi)
	suite.ValidateFileExists(&fi)

	response = uploadFunc("sample.txt", testServer.URL+"/upload")
	suite.A.Equal(http.StatusBadRequest, response.StatusCode)

}
