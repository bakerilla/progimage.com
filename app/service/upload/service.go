package upload

import (
	"context"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface"
	"github.com/go-kit/kit/log"
	"github.com/go-xorm/xorm"
	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
	"progimage.com/app/models"
	s "progimage.com/app/service"
	"progimage.com/utils/s3"
	tx "progimage.com/utils/transaction"
)

type service struct {
	db       xorm.Interface
	uploader s3manageriface.UploaderAPI
}

type upload interface {
	s.Service
	UploadFile(ctx context.Context, l *Localfile) (*models.FileInfo, error)
	FetchFile(ctx context.Context, f *FetchUrl) (*models.FileInfo, error)
}

func NewService(db xorm.Interface, uploader s3manageriface.UploaderAPI) upload {
	return newService(db, uploader)
}
func newService(db xorm.Interface, uploader s3manageriface.UploaderAPI) *service {
	return &service{db: db, uploader: uploader}
}

func (svc *service) Route(mainRouter *mux.Router, log log.Logger) {
	svc.newRouter(mainRouter, log)

}

func (svc *service) newFileEntry(fileinfo *models.FileInfo) error {
	_, err := svc.db.Table(models.FileInfoTableName).Insert(fileinfo)
	return err
}

func (svc *service) FetchFile(ctx context.Context, fetcher *FetchUrl) (*models.FileInfo, error) {
	fileinfo := &models.FileInfo{
		ID:          uuid.Must(uuid.NewV4()).String(),
		DateCreated: time.Now().UTC(),
		Source:      "fetch",
	}

	if err := svc.newFileEntry(fileinfo); err != nil {
		return &models.FileInfo{}, err
	}
	fetcher.uuid = fileinfo.ID
	fetcher.uploader = svc.uploader

	if err := tx.Get(fetcher); err != nil {
		return nil, err
	}
	fileinfo.Uploaded = true
	if _, err := svc.db.Table(models.FileInfoTableName).Where("id = ?", fileinfo.ID).Cols("uploaded").Update(fileinfo); err != nil {
		return &models.FileInfo{}, err
	}
	return fileinfo, nil
}

func (svc *service) UploadFile(ctx context.Context, l *Localfile) (*models.FileInfo, error) {
	f, err := os.Open(l.StoredPath)
	if err != nil {
		return &models.FileInfo{
			FileName: l.OriginalName,
			Source:   "upload",
		}, err
	}
	defer f.Close()
	fileinfo := &models.FileInfo{
		FileName:    l.OriginalName,
		ID:          uuid.Must(uuid.NewV4()).String(),
		DateCreated: time.Now().UTC(),
		Source:      "upload",
	}

	// make sure can create an entry in db
	if err := svc.newFileEntry(fileinfo); err != nil {
		return &models.FileInfo{
			FileName: l.OriginalName,
		}, err
	}

	if err := s3.UploadBuffer(svc.uploader, s.ServiceBucket, fileinfo.ID, f); err != nil {
		return &models.FileInfo{
			FileName: l.OriginalName,
		}, err
	}
	fileinfo.Uploaded = true
	if _, err = svc.db.Table(models.FileInfoTableName).Where("id = ?", fileinfo.ID).Cols("uploaded").Update(fileinfo); err != nil {
		return &models.FileInfo{
			FileName: l.OriginalName,
		}, err
	}
	os.Remove(l.StoredPath)

	return fileinfo, nil
}
