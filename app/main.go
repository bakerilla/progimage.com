package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/go-kit/kit/log"
	"github.com/go-xorm/xorm"

	"github.com/gorilla/mux"
	"github.com/jasonlvhit/gocron"
	_ "github.com/lib/pq"
	nr "github.com/newrelic/go-agent/v3/newrelic"
	"progimage.com/app/background/batchprocess"
	"progimage.com/app/service"
	"progimage.com/app/service/batch"
	"progimage.com/app/service/download"
	"progimage.com/app/service/example"
	"progimage.com/app/service/upload"
	"progimage.com/utils/db"
)

const (
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbName   = "progimage"
)

var (
	host      = "localhost"
	tmpDir    string
	minioHost = "localhost"
	minioPort = "9000"
)

//AWS
var (
	Region          = "us-west-1"
	AccessKeyId     = "minio"
	SecretAccessKey = "minio123"
	s3Sesh          *session.Session
	engine          *xorm.Engine
)

func init() {
	var err error
	flag.StringVar(&tmpDir, "td", "tmp-images", "directory where tmp files get saved")
	flag.Parse()
	if err := os.MkdirAll(tmpDir+"/batch", 0750); err != nil {
		if os.IsExist(err) {

		} else {
			panic(err)
		}

	}
	upload.TmpDir = tmpDir
	batchprocess.TmpDir = tmpDir + "/batch"

	//database
	if h := os.Getenv("PGHOST"); h != "" {
		host = h
	}
	if engine, err = db.New(host, user, password, dbName, port); err != nil {
		panic(err)
	}
	if m := os.Getenv("MINIOHOST"); m != "" {
		minioHost = m
	}

	// AWS
	if region := os.Getenv("AWS_REGION"); region != "" {
		Region = region
	}
	if accessKeyID := os.Getenv("AWS_ACCESS_KEY_ID"); accessKeyID != "" {
		AccessKeyId = accessKeyID
	}
	if secretAccessKey := os.Getenv("AWS_SECRET_ACCESS_KEY"); secretAccessKey != "" {
		SecretAccessKey = secretAccessKey
	}
	creds := credentials.NewStaticCredentials(AccessKeyId, SecretAccessKey, "")

	if s3Sesh, err = session.NewSession(&aws.Config{
		Endpoint: aws.String(
			fmt.Sprintf(
				"http://%s:%v",
				minioHost,
				minioPort,
			),
		),
		Region:           aws.String("us-east-1"),
		DisableSSL:       aws.Bool(true),
		S3ForcePathStyle: aws.Bool(true),
		Credentials:      creds,
	}); err != nil {
		panic(err)
	}
	//bucket
	service.InitializeBucket(s3Sesh)
}

func main() {

	defer engine.Close()
	addr := ":8000"
	logger := log.NewLogfmtLogger(os.Stderr)
	apiLogger := log.With(logger, "listen", addr, "caller", log.DefaultCaller)
	r := mux.NewRouter()

	uploader := s3manager.NewUploader(s3Sesh)
	downloader := s3manager.NewDownloader(s3Sesh)

	// bgProcess := batchprocess.New(engine, uploader, downloader, log.With(logger, "service", "bgProcess"))

	upload.NewService(engine, uploader).Route(r, log.With(apiLogger, "service", "upload"))
	download.NewService(engine, downloader).Route(r, log.With(apiLogger, "service", "download"))
	batch.NewService(engine, downloader).Route(r, log.With(apiLogger, "service", "batch"))

	sqlx := db.SqlxMust(host, user, password, dbName, port)
	app, _ := nr.NewApplication()
	example.New(app, sqlx).Route(r, log.With(apiLogger, "service", "example"))

	srv := &http.Server{
		Handler: r,
		Addr:    addr,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	go func() {
		s := gocron.NewScheduler()
		// s.Every(2).Seconds().Do(bgProcess.RunTransformer)
		<-s.Start()
	}()
	go func() {
		srv.ListenAndServe()
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	select {
	case <-quit:
		logger.Log("event", "interrupt")
	}

}
