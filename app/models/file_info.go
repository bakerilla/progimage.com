package models

import "time"

const FileInfoTableName = "file_info"

type FileInfo struct {
	ID          string    `xorm:"id" json:"id"`
	DateCreated time.Time `xorm:"date_created" json:"date_created"`
	Uploaded    bool      `xorm:"uploaded" json:"-"`
	FileName    string    `xorm:"-" json:"file_name"`
	Source      string    `xorm:"source" json:"source"`
}
