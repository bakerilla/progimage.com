package models

type FileTransformations struct {
	TransformationType string `xorm:"transformation_type"`
	Parameters         string `xorm:"parameters"`
	DateCreated        string `xorm:"date_created"`
}
