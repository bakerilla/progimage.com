package models

import (
	"time"

	"progimage.com/app/transform"
)

// CREATE TYPE batch_status as ENUM('pending','started', 'error', 'completed', 'ready');
// CREATE TABLE batch_transformation(
//    id uuid,
//    extension text,
//    parameters json,
//    file_ids json,
//    date_created timestamptz,
//    bstatus batch_status,
//    PRIMARY KEY(id)
// );

const BatchTransformationTableName = `batch_transformation`

type BatchTransformation struct {
	ID          string                       `xorm:"id" json:"id"`
	Extension   string                       `xorm:"extension" json:"extension"`
	Parameters  transform.ExtendedParameters `xorm:"parameters" json:"-"`
	FileIDs     []string                     `xorm:"file_ids" json:"-"`
	DateCreated time.Time                    `xorm:"date_created" json:"date_created"`
	Bstatus     string                       `xorm:"bstatus" json:"status"`
}
