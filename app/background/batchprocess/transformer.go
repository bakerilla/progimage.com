package batchprocess

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface"
	"github.com/disintegration/imaging"
	"github.com/go-xorm/xorm"
	_ "github.com/lib/pq"
	"progimage.com/app/models"
	"progimage.com/app/service"
	"progimage.com/app/transform"
	"progimage.com/utils/s3"
)

const batchSize int = 10

type transformer struct {
	list     []*models.BatchTransformation
	complete bool
}

// Run ... the most basic. runs transformation on batches
func (me *Process) RunTransformer() error {
	var (
		list []*models.BatchTransformation
	)
	count, err := me.db.Table(models.BatchTransformationTableName).Where("bstatus=?", "pending").Limit(batchSize).FindAndCount(&list)
	if err != nil {
		me.log.Log("fetch_error", err.Error())
		return err
	}
	me.log.Log("returned count", count)
	if count == 0 {
		return err
	}

	me.transformer = transformer{
		list: list,
	}
	//flag as started
	if err := me.transformer.updateBatchStatus(me.db, "started"); err != nil {
		me.log.Log("toggle_error", err.Error())
		return err
	}

	me.transformer.handleBatches(me.db, me.uploader, me.downloader)
	me.transformer.complete = true

	return nil
}

func (me *transformer) updateBatchStatus(db *xorm.Engine, status string) error {
	var (
		aliasArr []string

		args = []interface{}{
			status,
		}
	)
	for _, v := range me.list {
		aliasArr = append(aliasArr, "?")
		args = append(args, v.ID)
	}
	query := fmt.Sprintf("with batch as (select ?::batch_status ss) update %s set bstatus=batch.ss from batch where id in(%s)",
		models.BatchTransformationTableName,
		strings.Join(aliasArr, ", "),
	)

	fmt.Println("args", args)

	g := []interface{}{
		query,
	}
	g = append(g, args...)

	// fmt.Println("query", query, args)
	if _, err := db.Exec(g...); err != nil {
		fmt.Println("err", err)
		return err
	}
	return nil
}

type handleBatchError struct {
	errors  error
	BatchID string
}

func (me *transformer) handleBatches(db *xorm.Engine, uploader s3manageriface.UploaderAPI, downloader s3manageriface.DownloaderAPI) error {

	channelDone := make(chan string)
	err := make(chan handleBatchError)
	success := make(chan string)
	for _, job := range me.list {
		go handleBatchAsync(uploader, downloader, channelDone, success, err, job)
	}

	for i := 0; i < len(me.list); {
		select {
		case x := <-err:

			fmt.Println("err at batch ID: ", x.BatchID, x.errors.Error())
			updateBatchStatus(db, x.BatchID, `error`)
		case successfulID := <-success:
			fmt.Println("batch ID succeeded:", successfulID)
			updateBatchStatus(db, successfulID, `ready`)
		case y := <-channelDone:
			fmt.Println("complete batch: ", y)
			i++

		}
	}
	return nil
}

// handles a single batch.id
func handleBatchAsync(uploader s3manageriface.UploaderAPI, downloader s3manageriface.DownloaderAPI, complete, succeed chan string, ch chan handleBatchError, batch *models.BatchTransformation) {
	defer func() {
		complete <- batch.ID
	}()
	//create directory
	if _, err := os.Stat(TmpDir + "/" + batch.ID); os.IsNotExist(err) {
		if err := os.Mkdir(TmpDir+"/"+batch.ID, 0750); err != nil {
			fmt.Println("mnkdir")
			ch <- handleBatchError{
				BatchID: batch.ID,
				errors:  err,
			}
			return
		}
	} else {
		ch <- handleBatchError{
			BatchID: batch.ID,
			errors:  errors.New("path already exists"),
		}
		return
	}

	channelDone := make(chan string)
	err := make(chan transformFileError)

	for _, file := range batch.FileIDs {
		go transformFile(uploader, downloader, channelDone, err, batch.ID, file, batch.Extension, &batch.Parameters)
	}
	for i := 0; i < len(batch.FileIDs); {
		select {
		case x := <-err:
			fmt.Println("error at fileID: ", x.fileID) // based on x, write to file or whatever then
			ch <- handleBatchError{
				BatchID: batch.ID,
				errors:  x.err,
			}
			return
		case y := <-channelDone:
			fmt.Println("complete file: ", y, "if batch: ", batch.ID)
			i++

		}
	}
	// zip the entire folder and upload to s3 if no error, the succeed
	succeed <- batch.ID

}

type transformFileError struct {
	err    error
	fileID string
}

func transformFile(uploader s3manageriface.UploaderAPI, downloader s3manageriface.DownloaderAPI, done chan string, err chan transformFileError, batchID, fileID, extension string, params *transform.ExtendedParameters) {
	defer func() {
		done <- fileID
	}()

	// load file
	data, e := s3.Download(downloader, service.ServiceBucket, fileID)
	if e != nil {
		err <- transformFileError{
			fileID: fileID,
			err:    e,
		}
		return
	}
	tx, e := transform.New(data)
	if e != nil {
		err <- transformFileError{
			fileID: fileID,
			err:    e,
		}
		return
	}
	//transform file
	tx.Transform(params)
	fm, e := imaging.FormatFromExtension(extension)
	if e != nil {
		err <- transformFileError{
			fileID: fileID,
			err:    e,
		}
		return
	}
	_, e = tx.Encode(fm)
	if e != nil {
		err <- transformFileError{
			fileID: fileID,
			err:    e,
		}
		return
	}

	// save file to tmp location
	// if successful then ok

}
