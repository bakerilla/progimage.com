package batchprocess

import (
	"fmt"

	"github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface"
	"github.com/go-kit/kit/log"
	"github.com/go-xorm/xorm"
	"progimage.com/app/models"
)

var TmpDir = "tmp"

type Process struct {
	db          *xorm.Engine
	downloader  s3manageriface.DownloaderAPI
	uploader    s3manageriface.UploaderAPI
	log         log.Logger
	transformer transformer
}

func New(db *xorm.Engine, uploader s3manageriface.UploaderAPI, downloader s3manageriface.DownloaderAPI, log log.Logger) *Process {
	return &Process{
		db:         db,
		uploader:   uploader,
		downloader: downloader,
		log:        log,
	}
}

func updateBatchStatus(db *xorm.Engine, id, status string) error {
	var (
		args = []interface{}{
			fmt.Sprintf(`update %s set bstatus=? where id=?`, models.BatchTransformationTableName),
			status,
			id,
		}
	)
	_, err := db.Exec(args...)
	return err
}
