# USAGE #
## to run: ## 
Run:

```
make dockerup
```
## directory structure ##
1. app/  - application directory. entrypoint: main.go 
    - e2e/ - e2e testing that covers upload, download endpoints + testcasses
2. migrate/ - db migration directory. entrypoint: main 

## api endpoints ##
### POST /upload ###
Content-Type: multipart/form-data

Body: 

file: {file to be uoloaded}

Response on success:
```
{
    "id": "a113b991-ce8f-4bad-b036-b67e9a47f08b",
    "date_created": "2021-06-16T08:11:42.9631363Z",
    "file_name": "",
    "source": "upload"
}
```

### GET /fetch ###

Query:

url: {url of image to be uploaded}

Response on success:
```
{
    "id": "a113b991-ce8f-4bad-b036-b67e9a47f08b",
    "date_created": "2021-06-16T08:11:42.9631363Z",
    "file_name": "",
    "source": "fetch"
}
```


### GET /download/{fileID}.{extension} ###

Path:

fileID: unique ID resulting from /upload or /fetch (string)

extension: file extension to download. eg jpeg, png (string)


### GET /download/{transformation}/{fileID}.{extension} ###

Path:

fileID: unique ID resulting from /upload or /fetch (string)

trandformation: transformation. comma-separated transformations, eg: crop_c,height_100,width_100

extension: file extension to download. eg jpeg, png (string)

# SCOPE #

UPLOAD 
1. via Form Post -- POST /upload
    - validate that the uploaded object is an image 
2. via provided URL -- GET /fetch?url=...
    - validate that the provided endpoint is an image 
    - out of scope: doesn't check if the endpoint is private. will just die


DOWNLOAD
1. Downloads the original image -- GET /{id}.{extension}
    - extension validation  
    - id validation 
    - checks if ID exists
2. Downloads a transformed imahe -- GET /{transformation}/{id}.{extension}
    - extension validation  
    - id validation 
    - ensures that the transformation string is accurate
    - checks if ID exists

TRANSFORMATION
1. CROP CENTRE      crop_c,height_$x,width_$y
2. RESIZE           height_$x,width_$y
3. BLUR             blur_$sigma
4. SHARPEN          sharpen_$sigma
5. ROTATE           rotate_xxx
6. GAMMA            gamma_xxx
7. CONTRAST         contrast_xxx



# OUT OF SCOPE #
1. batch-processing


