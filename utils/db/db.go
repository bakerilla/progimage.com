package db

import (
	"database/sql"
	"fmt"

	"github.com/go-xorm/xorm"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	_ "github.com/newrelic/go-agent/v3/integrations/nrpq"
)

func New(host, user, password, dbName string, port int) (*xorm.Engine, error) {
	psqlInfo := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host,
		port, user, password, dbName)

	engine, err := xorm.NewEngine("postgres", psqlInfo)
	if err != nil {
		return nil, err
	}
	return engine, nil
}

func SQLX(host, user, password, dbName string, port int) (*sqlx.DB, error) {
	dsn := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host,
		port, user, password, dbName)

	db, err := sql.Open("nrpostgres", dsn)
	if err != nil {
		return nil, err
	}
	return sqlx.NewDb(db, "postgres"), nil
}
func SqlxMust(host, user, password, dbName string, port int) *sqlx.DB {
	db, err := SQLX(host, user, password, dbName, port)
	if err != nil {
		panic(err)
	}
	return db
}
