package transaction

import (
	"io"
	"net/http"
)

type Transaction struct {
	Endpoint string
}
type Interface interface {
	URL() string
	ParseHeader(resp http.Header)
	ParseBody(rc io.ReadCloser) error
	StatusCode(sc int)
}

// HEAD does a HEAD http call
func Head(t Interface) error {
	req, e := http.NewRequest("HEAD", t.URL(), nil)
	if e != nil {
		panic(e)
	}
	res, err := new(http.Client).Do(req)
	if err != nil {
		return err
	}
	t.ParseHeader(res.Header)
	t.StatusCode(res.StatusCode)
	return nil
}

func Get(t Interface) error {
	req, e := http.NewRequest("GET", t.URL(), nil)
	if e != nil {
		panic(e)
	}
	res, err := new(http.Client).Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	t.StatusCode(res.StatusCode)
	t.ParseHeader(res.Header)
	return t.ParseBody(res.Body)
}
