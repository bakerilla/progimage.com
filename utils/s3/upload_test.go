package s3_test

import (
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface"
	"github.com/stretchr/testify/assert"
	"progimage.com/utils/s3"
)

type upper struct {
	s3manageriface.UploaderAPI
}

func (u upper) Upload(input *s3manager.UploadInput, options ...func(*s3manager.Uploader)) (*s3manager.UploadOutput, error) {

	return nil, nil
}

func (m *upper) UploadWithContext(aws.Context, *s3manager.UploadInput, ...func(*s3manager.Uploader)) (*s3manager.UploadOutput, error) {
	return nil, nil
}

func TestUpload(t *testing.T) {
	uploader := &upper{}
	assert.NoError(t, s3.UploadBuffer(uploader, "bukcey", "key", nil))
	assert.NoError(t, s3.Upload(uploader, "bukcey", "key", nil))
}
