package s3_test

import (
	"io"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface"
	"github.com/stretchr/testify/assert"
	s "progimage.com/utils/s3"
)

type downer struct {
	s3manageriface.DownloaderAPI
}

func (d *downer) Download(a io.WriterAt, b *s3.GetObjectInput, c ...func(*s3manager.Downloader)) (int64, error) {
	return 100, nil
}

func (d *downer) DownloadWithContext(aws.Context, io.WriterAt, *s3.GetObjectInput, ...func(*s3manager.Downloader)) (int64, error) {
	return 0, nil
}

func TestDownload(t *testing.T) {
	downloader := &downer{}
	_, err := s.Download(downloader, "bukcey", "key")
	assert.NoError(t, err)
}
