package s3

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface"
)

func Download(dl s3manageriface.DownloaderAPI, bucket, key string) ([]byte, error) {
	buff := &aws.WriteAtBuffer{}
	input := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}
	if _, err := dl.Download(buff, input); err != nil {
		return nil, err
	}
	return buff.Bytes(), nil

}
